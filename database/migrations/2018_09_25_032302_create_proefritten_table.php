<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProefrittenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proefritten', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('license_plate',30)->unsigned()->nullable();
            $table->string('license_number',30)->nullable();
            $table->date('date')->nullable();
            $table->string('begin_location',50)->nullable();
            $table->string('end_location',50)->nullable();
            $table->string('start_time',10)->nullable();
            $table->string('end_time',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proefritten');
    }
}
